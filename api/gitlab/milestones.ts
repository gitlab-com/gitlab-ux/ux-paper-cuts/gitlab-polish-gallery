import { request, gql } from "graphql-request";
import { API_URL, FROM_DATE } from "../constants";

interface PageInfo {
  hasNextPage: boolean;
  endCursor: string;
}

export interface Milestone {
  id: string;
  title: string;
  startDate: string;
  dueDate: string;
}

interface MilestoneEdge {
  node: Milestone;
}

interface MilestonesResponse {
  edges: MilestoneEdge[];
  pageInfo: PageInfo;
}

interface MilestoneQueryData {
  group: {
    milestones: MilestonesResponse;
  };
}

const QUERY = gql`
  query ($start: Date!, $end: Date!, $after: String!) {
    group(fullPath: "gitlab-org") {
      milestones(
        sort: DUE_DATE_DESC
        timeframe: { start: $start, end: $end }
        after: $after
      ) {
        edges {
          node {
            id
            title
            startDate
            dueDate
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;

export async function getAllMilestones() {
  const endDate = new Date().toISOString().split("T")[0];

  const milestones: Milestone[] = [];
  let pageInfo: PageInfo = {
    hasNextPage: true,
    endCursor: "",
  };

  while (pageInfo.hasNextPage) {
    const response: MilestoneQueryData = await request(API_URL, QUERY, {
      start: FROM_DATE,
      end: endDate,
      after: pageInfo.endCursor,
    });

    pageInfo = response.group.milestones.pageInfo;
    response.group.milestones.edges.forEach((edge) =>
      milestones.push(edge.node)
    );
  }

  return milestones;
}
