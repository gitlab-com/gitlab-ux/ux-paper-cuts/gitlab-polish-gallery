import { expect, test } from "bun:test";
import PaperCut from "./papercut";

const MOCK_MERGE_REQUEST = {
  id: "gid://gitlab/MergeRequest/238315111",
  title: "Add spacing between approval checkbox and textarea",
  username: "annabeldunstone",
  avatarUrl:
    "https://secure.gravatar.com/avatar/85c0203926134720f9a9aa8c6e264c78?s=80&d=identicon",
  milestone: "16.3",
  url: "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127233",
  descriptionHtml:
    '<h2 data-sourcepos="1:1-1:32" dir="auto">&#x000A;<a id="user-content-what-does-this-mr-do-and-why" class="anchor" href="#what-does-this-mr-do-and-why" aria-hidden="true"></a>What does this MR do and why?</h2>&#x000A;<ul data-sourcepos="2:1-5:0" dir="auto">&#x000A;<li data-sourcepos="2:1-5:0">Fix alignment of <code>Approve</code> checkbox on summary comment dropdown</li>&#x000A;</ul>&#x000A;<h2 data-sourcepos="6:1-6:35" dir="auto">&#x000A;<a id="user-content-screenshots-or-screen-recordings" class="anchor" href="#screenshots-or-screen-recordings" aria-hidden="true"></a>Screenshots or screen recordings</h2>&#x000A;<table data-sourcepos="8:1-11:249" dir="auto">&#x000A;<thead>&#x000A;<tr data-sourcepos="8:1-8:18">&#x000A;<th data-sourcepos="8:2-8:9">Before</th>&#x000A;<th data-sourcepos="8:11-8:17">After</th>&#x000A;</tr>&#x000A;</thead>&#x000A;<tbody>&#x000A;<tr data-sourcepos="10:1-10:249">&#x000A;<td data-sourcepos="10:2-10:124"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.39.30_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png" data-canonical-src="/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png"></a></td>&#x000A;<td data-sourcepos="10:126-10:248"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.34.32_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png" data-canonical-src="/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png"></a></td>&#x000A;</tr>&#x000A;<tr data-sourcepos="11:1-11:249">&#x000A;<td data-sourcepos="11:2-11:124"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.39.53_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png" data-canonical-src="/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png"></a></td>&#x000A;<td data-sourcepos="11:126-11:248"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.37.43_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png" data-canonical-src="/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png"></a></td>&#x000A;</tr>&#x000A;</tbody>&#x000A;</table>&#x000A;<h2 data-sourcepos="13:1-13:37" dir="auto">&#x000A;<a id="user-content-how-to-set-up-and-validate-locally" class="anchor" href="#how-to-set-up-and-validate-locally" aria-hidden="true"></a>How to set up and validate locally</h2>&#x000A;<ul data-sourcepos="15:1-19:0" dir="auto">&#x000A;<li data-sourcepos="15:1-15:10">Go to MR</li>&#x000A;<li data-sourcepos="16:1-16:16">Start a review</li>&#x000A;<li data-sourcepos="17:1-19:0">Click <code>Finish review</code>&#x000A;</li>&#x000A;</ul>&#x000A;<p data-sourcepos="20:1-20:53" dir="auto"><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/419344" data-reference-type="issue" data-original="https://gitlab.com/gitlab-org/gitlab/-/issues/419344+" data-link="false" data-link-reference="true" data-project="278964" data-issue="131123534" data-project-path="gitlab-org/gitlab" data-iid="419344" data-reference-format="+" data-issue-type="issue" data-container="body" data-placement="top" title="MR submit review: editor border is too close to `Approve` tickbox" class="gfm gfm-issue">MR submit review: editor border is too close to... (#419344 - closed)</a></p>&#x000A;<p data-sourcepos="22:1-22:53" dir="auto"><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/417912" data-reference-type="issue" data-original="https://gitlab.com/gitlab-org/gitlab/-/issues/417912+" data-link="false" data-link-reference="true" data-project="278964" data-issue="130734727" data-project-path="gitlab-org/gitlab" data-iid="417912" data-reference-format="+" data-issue-type="issue" data-container="body" data-placement="top" title="UX Paper Cuts 16.3 → Settings UI, part 2" class="gfm gfm-issue">UX Paper Cuts 16.3 → Settings UI, part 2 (#417912 - closed)</a></p>',
  labels: ["UX Paper Cuts", "other"],
  mergedAt: "2024-01-01",
};

const MOCK_MERGE_REQUEST_WITH_VIDEO = {
  id: "gid://gitlab/MergeRequest/238315199",
  title: "Foobar",
  username: "clavimoniere",
  avatarUrl:
    "https://secure.gravatar.com/avatar/85c0203926134720f9a9aa8c6e264c78?s=80&d=identicon",
  milestone: "16.3",
  url: "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1279999",
  descriptionHtml: `<div class="description !gl-mt-4 js-task-list-container is-task-list-enabled" data-testid="description-content">
<div class="md">
<h2 data-sourcepos="1:1-1:32" dir="auto">
<a href="#what-does-this-mr-do-and-why" aria-hidden="true" class="anchor" id="user-content-what-does-this-mr-do-and-why"></a>What does this MR do and why?</h2>
<p data-sourcepos="3:1-3:55" dir="auto">Allow vertically resizing markdown and rich text fields</p>
<h2 data-sourcepos="5:1-5:35" dir="auto">
<a href="#screenshots-or-screen-recordings" aria-hidden="true" class="anchor" id="user-content-screenshots-or-screen-recordings"></a>Screenshots or screen recordings</h2>
<table data-sourcepos="7:1-9:277" dir="auto">
<thead>
<tr data-sourcepos="7:1-7:19">
<th data-sourcepos="7:2-7:9">Before</th>
<th data-sourcepos="7:11-7:18">After</th>
</tr>
</thead>
<tbody>
<tr data-sourcepos="9:1-9:277">
<td data-sourcepos="9:2-9:138"><span class="media-container video-container"><video src="/-/project/278964/uploads/18a0dbeecada5989b507084143b274c3/Screen_Recording_2024-10-17_at_12.22.34.mov" controls="true" data-setup="{}" data-title="Screen_Recording_2024-10-17_at_12.22.34" preload="metadata" data-canonical-src="/uploads/18a0dbeecada5989b507084143b274c3/Screen_Recording_2024-10-17_at_12.22.34.mov" class="gfm"></video><a href="/-/project/278964/uploads/18a0dbeecada5989b507084143b274c3/Screen_Recording_2024-10-17_at_12.22.34.mov" target="_blank" rel="noopener noreferrer" title="Download 'Screen_Recording_2024-10-17_at_12.22.34'" data-canonical-src="/uploads/18a0dbeecada5989b507084143b274c3/Screen_Recording_2024-10-17_at_12.22.34.mov" data-link="true" class="gfm">Screen_Recording_2024-10-17_at_12.22.34</a></span></td>
<td data-sourcepos="9:140-9:276"><span class="media-container video-container"><video src="/-/project/278964/uploads/49167e484f7a1069e3ff93dfe8c82e6f/Screen_Recording_2024-10-17_at_12.21.36.mov" controls="true" data-setup="{}" data-title="Screen_Recording_2024-10-17_at_12.21.36" preload="metadata" data-canonical-src="/uploads/49167e484f7a1069e3ff93dfe8c82e6f/Screen_Recording_2024-10-17_at_12.21.36.mov" class="gfm"></video><a href="/-/project/278964/uploads/49167e484f7a1069e3ff93dfe8c82e6f/Screen_Recording_2024-10-17_at_12.21.36.mov" target="_blank" rel="noopener noreferrer" title="Download 'Screen_Recording_2024-10-17_at_12.21.36'" data-canonical-src="/uploads/49167e484f7a1069e3ff93dfe8c82e6f/Screen_Recording_2024-10-17_at_12.21.36.mov" data-link="true" class="gfm">Screen_Recording_2024-10-17_at_12.21.36</a></span></td>
</tr>
</tbody>
</table>
<h2 data-sourcepos="12:1-12:37" dir="auto">
<a href="#how-to-set-up-and-validate-locally" aria-hidden="true" class="anchor" id="user-content-how-to-set-up-and-validate-locally"></a>How to set up and validate locally</h2>
<ol data-sourcepos="14:1-16:68" dir="auto">
<li data-sourcepos="14:1-14:24">Check out this branch</li>
<li data-sourcepos="15:1-15:96">In the GDK, create an issue, epic, or MR, or navigate to the Activity feed on an existing one</li>
<li data-sourcepos="16:1-16:68">You will be able to vertically reize the markdown/rich text input</li>
</ol>
</div>
</div>`,
  labels: ["UX Paper Cuts", "other"],
  mergedAt: "2024-01-01",
};

const MOCK_MERGE_REQUEST_WITHOUT_IMAGES = {
  id: "gid://gitlab/MergeRequest/238315111",
  title: "Add spacing between approval checkbox and textarea",
  username: "annabeldunstone",
  avatarUrl:
    "https://secure.gravatar.com/avatar/85c0203926134720f9a9aa8c6e264c78?s=80&d=identicon",
  milestone: "16.3",
  url: "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/127233",
  descriptionHtml:
    '<h2 data-sourcepos="1:1-1:32" dir="auto">&#x000A;<a id="user-content-what-does-this-mr-do-and-why" class="anchor" href="#what-does-this-mr-do-and-why" aria-hidden="true"></a>What does this MR do and why?</h2>&#x000A;<ul data-sourcepos="2:1-5:0" dir="auto">&#x000A;<li data-sourcepos="2:1-5:0">Fix alignment of <code>Approve</code> checkbox on summary comment dropdown</li>&#x000A;</ul>&#x000A;<h2 data-sourcepos="6:1-6:35" dir="auto">&#x000A;<a id="user-content-screenshots-or-screen-recordings" class="anchor" href="#screenshots-or-screen-recordings" aria-hidden="true"></a>Screenshots or screen recordings</h2>&#x000A;<table data-sourcepos="8:1-11:249" dir="auto">&#x000A;<thead>&#x000A;<tr data-sourcepos="8:1-8:18">&#x000A;<th data-sourcepos="8:2-8:9">Other</th>&#x000A;<th data-sourcepos="8:11-8:17">Heading</th>&#x000A;</tr>&#x000A;</thead>&#x000A;<tbody>&#x000A;<tr data-sourcepos="10:1-10:249">&#x000A;<td data-sourcepos="10:2-10:124"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.39.30_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png" data-canonical-src="/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png"></a></td>&#x000A;<td data-sourcepos="10:126-10:248"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.34.32_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png" data-canonical-src="/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png"></a></td>&#x000A;</tr>&#x000A;<tr data-sourcepos="11:1-11:249">&#x000A;<td data-sourcepos="11:2-11:124"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.39.53_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png" data-canonical-src="/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png"></a></td>&#x000A;<td data-sourcepos="11:126-11:248"><a class="no-attachment-icon gfm" href="/gitlab-org/gitlab/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png" target="_blank" rel="noopener noreferrer" data-canonical-src="/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png" data-link="true"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" alt="Screenshot_2023-07-21_at_6.37.43_AM" decoding="async" class="lazy gfm" data-src="/gitlab-org/gitlab/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png" data-canonical-src="/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png"></a></td>&#x000A;</tr>&#x000A;</tbody>&#x000A;</table>&#x000A;<h2 data-sourcepos="13:1-13:37" dir="auto">&#x000A;<a id="user-content-how-to-set-up-and-validate-locally" class="anchor" href="#how-to-set-up-and-validate-locally" aria-hidden="true"></a>How to set up and validate locally</h2>&#x000A;<ul data-sourcepos="15:1-19:0" dir="auto">&#x000A;<li data-sourcepos="15:1-15:10">Go to MR</li>&#x000A;<li data-sourcepos="16:1-16:16">Start a review</li>&#x000A;<li data-sourcepos="17:1-19:0">Click <code>Finish review</code>&#x000A;</li>&#x000A;</ul>&#x000A;<p data-sourcepos="20:1-20:53" dir="auto"><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/419344" data-reference-type="issue" data-original="https://gitlab.com/gitlab-org/gitlab/-/issues/419344+" data-link="false" data-link-reference="true" data-project="278964" data-issue="131123534" data-project-path="gitlab-org/gitlab" data-iid="419344" data-reference-format="+" data-issue-type="issue" data-container="body" data-placement="top" title="MR submit review: editor border is too close to `Approve` tickbox" class="gfm gfm-issue">MR submit review: editor border is too close to... (#419344 - closed)</a></p>&#x000A;<p data-sourcepos="22:1-22:53" dir="auto"><a href="https://gitlab.com/gitlab-org/gitlab/-/issues/417912" data-reference-type="issue" data-original="https://gitlab.com/gitlab-org/gitlab/-/issues/417912+" data-link="false" data-link-reference="true" data-project="278964" data-issue="130734727" data-project-path="gitlab-org/gitlab" data-iid="417912" data-reference-format="+" data-issue-type="issue" data-container="body" data-placement="top" title="UX Paper Cuts 16.3 → Settings UI, part 2" class="gfm gfm-issue">UX Paper Cuts 16.3 → Settings UI, part 2 (#417912 - closed)</a></p>',
  labels: ["UX Paper Cuts", "other"],
  mergedAt: "2024-01-01",
};

test("returns papercut from merge request", () => {
  const paperCut = PaperCut.fromMergeRequest(MOCK_MERGE_REQUEST);

  expect(paperCut).not.toBeNull();

  expect(paperCut?.mergeRequestId).toEqual(MOCK_MERGE_REQUEST.id);
  expect(paperCut?.title).toEqual(MOCK_MERGE_REQUEST.title);
  expect(paperCut?.labels).toEqual(["UX Paper Cuts"]);
  expect(paperCut?.images).toEqual([
    {
      before: {
        type: "img",
        src: "https://gitlab.com/gitlab-org/gitlab/uploads/ac5efc2b732b65f632fca6913853dec6/Screenshot_2023-07-21_at_6.39.30_AM.png",
      },
      after: {
        type: "img",
        src: "https://gitlab.com/gitlab-org/gitlab/uploads/4f65161155ece554bf8d0817f1691bb1/Screenshot_2023-07-21_at_6.34.32_AM.png",
      },
    },
    {
      before: {
        type: "img",
        src: "https://gitlab.com/gitlab-org/gitlab/uploads/a71e5dd1c553420b2cb06c69d0124b08/Screenshot_2023-07-21_at_6.39.53_AM.png",
      },
      after: {
        type: "img",
        src: "https://gitlab.com/gitlab-org/gitlab/uploads/29f5e4fd61b8d3a1b17a1dbe202c2282/Screenshot_2023-07-21_at_6.37.43_AM.png",
      },
    },
  ]);
});

test("returns papercut with video data from merge request", () => {
  const paperCut = PaperCut.fromMergeRequest(MOCK_MERGE_REQUEST_WITH_VIDEO);

  expect(paperCut?.images).toEqual([
    {
      before: {
        src: "https://gitlab.com/-/project/278964/uploads/18a0dbeecada5989b507084143b274c3/Screen_Recording_2024-10-17_at_12.22.34.mov",
        type: "video",
      },
      after: {
        src: "https://gitlab.com/-/project/278964/uploads/49167e484f7a1069e3ff93dfe8c82e6f/Screen_Recording_2024-10-17_at_12.21.36.mov",
        type: "video",
      },
    },
  ]);
});

test("returns null when no before/after image is in the merge request", () => {
  const paperCut = PaperCut.fromMergeRequest(MOCK_MERGE_REQUEST_WITHOUT_IMAGES);

  expect(paperCut).toBeNull();
});
