import { LABELS } from "./constants";
import { MergeRequest } from "./gitlab/merge_requests";
import { Cheerio, CheerioAPI, Element, load } from "cheerio";

interface ImageSet {
  before: {
    type: "img" | "video";
    src: string;
  };
  after: {
    type: "img" | "video";
    src: string;
  };
}

export default class PaperCut {
  mergeRequestId: string;
  title: string;
  username: string;
  avatarUrl: string;
  url: string;
  milestone: string;
  images: ImageSet[];
  labels: string[];

  static fromMergeRequest(mergeRequest: MergeRequest): PaperCut | null {
    const images = getBeforeAndAfterImages(mergeRequest.descriptionHtml);

    if (images.length === 0) {
      return null;
    }

    return new PaperCut(mergeRequest, images);
  }

  private constructor(mergeRequest: MergeRequest, images: ImageSet[]) {
    this.mergeRequestId = mergeRequest.id;
    this.title = mergeRequest.title;
    this.avatarUrl = mergeRequest.avatarUrl;
    this.username = mergeRequest.username;
    this.url = mergeRequest.url;
    this.images = images;
    this.milestone = mergeRequest.milestone;
    this.labels = mergeRequest.labels.filter((label) =>
      Object.keys(LABELS).includes(label)
    );
  }
}

function getBeforeAndAfterImages(descriptionHtml: string): ImageSet[] {
  const doc = load(descriptionHtml);

  if (doc("table").length < 1) {
    return [];
  }

  const imageSets = [] as ImageSet[];

  doc("table").each((_, table) => {
    const headers = getHeaders(doc, table);
    if (headers.includes("before") || headers.includes("after")) {
      doc(table)
        .find("tr")
        .each((_, el) => {
          const row = doc(el);
          const imageSet = {} as ImageSet;

          row.find("td").each((index, td) => {
            const header = headers[index];
            const imgSrc = getImageSrc(doc(td));
            if (imgSrc) {
              imageSet[header] = { type: "img", src: imgSrc };
            } else {
              const vidSrc = getVidSrc(doc(td));
              if (vidSrc) {
                imageSet[header] = { type: "video", src: vidSrc };
              }
            }
          });

          if (Object.keys(imageSet).length) {
            imageSets.push(imageSet);
          }
        });
    }
  });

  return imageSets;
}

function getHeaders(doc: CheerioAPI, table: Element) {
  return doc(table)
    .find("th")
    .map((_, el) => doc(el).text().toLowerCase())
    .get();
}

function getImageSrc(td: Cheerio<Element>): string | null {
  const img = td.find("img");
  if (!img.length) return null;

  const src = img.attr("data-src");
  if (!src) return null;

  return src.startsWith("http") ? src : `https://gitlab.com${src}`;
}

function getVidSrc(td: Cheerio<Element>): string | null {
  const vid = td.find("video");
  if (!vid.length) return null;

  const src = vid.attr("src");
  if (!src) return null;

  return src.startsWith("http") ? src : `https://gitlab.com${src}`;
}
