# Papercuts

Hello 👋 this is a project to collect all the great small UI improvements we do at GitLab.
If you have questions, want to help (e.g. sponsor a nicer design for the page 😁) or have ideas how to improve, please create an issue or ping the UX Paper Cuts team.

[Check out the gallery](https://papercuts.gitlab.com)

## How does it work?

All Merge Requests that have the label `UI polish`, `Beautify our UI` or `UX Paper Cuts` and contain before and after images in a table will show up here. Here is an example MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/53213.

You can use normal markdown tables, as long as you mark "Before" and "After" columns correctly. There is also no limit on the amount of rows.

## Development

### Getting started

- [Install bun](https://bun.sh/)
- [Install yarn](https://yarnpkg.com/)

```sh
scripts/dev

# Optionally pass a different start date
# scripts/dev 2024-03-01
```

Your local will be available at http://localhost:5173/

### Contributions

Contributions are welcome, see [CONTRIBUTING.MD](CONTRIBUTING.MD) for more info.